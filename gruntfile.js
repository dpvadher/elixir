module.exports = function(grunt) {

    // 1. All configuration goes here 
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        sass:{
            dist: {
                options: {
                    style: 'compressed'
                },
                files: {
                    'css/production.css': ['css/styles.scss']
                }
            } 
        },
        cssmin: {
          combine: {
            files: {
                'css/production.min.css': [
                    'css/bootstrap.min.css', 
                    'css/bootstrap-select.min.css', 
                    'css/icheck.css',
                    'css/jquery.nouislider.min.css',
                    'css/datepicker.css',
                    'css/jquery.jscrollpane.codrops.css',
                    'css/prettyPhoto.css',
                    'css/touch.css',
                    'css/margin.css',
                    'css/custom-slider.css',
                    'css/production.css'
                ]
            }
          }
        },
        watch: {
            sass:{
                files: 'css/*.scss',
                tasks: ['sass']
            }
            // ,cssmin:{
            //     files: 'css/*.css',
            //     tasks: ['cssmin']
            // }
        }

    });

    // 3. Where we tell Grunt we plan to use this plug-in.
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-watch');
    // grunt.registerTask('watch', [ 'watch']);
    // 4. Where we tell Grunt what to do when we type "grunt" into the terminal.
    grunt.registerTask('default', ['sass','cssmin']);

};