var app = angular.module('app', ['ui.grid', 'ui.grid.pagination']);
 
app.controller('MainCtrl', ['$scope', '$http', function ($scope, $http) {
  $scope.gridOptions1 = {
    paginationPageSizes: [25, 50, 75],
    paginationPageSize: 25,
    columnDefs: [
      { name: 'Lead Title' },
      { name: 'Value' },
      { name: 'Stage' },
      { name: 'Company' },
      { name: 'Contact' },
      { name: 'Expected Close Date' }
    ]
  };
 
  $scope.gridOptions2 = {
    enablePaginationControls: false,
    paginationPageSize: 25,
    columnDefs: [
      { name: 'Lead Title' },
      { name: 'Value' },
      { name: 'Stage' },
      { name: 'Company' },
      { name: 'Contact' },
      { name: 'Expected Close Date' }
    ]
  };
 
  $scope.gridOptions2.onRegisterApi = function (gridApi) {
    $scope.gridApi2 = gridApi;
  }
 
  var testData = [
    {
        "Lead Title": "Windows 10 Deal",
        "Value" : "Rs 1,98,000",
        "Stage" : "Leads",
        "Company" : "Microsoft",
        "Contact" : "Anju Yadav",
        "Expected Close Date" : "29th January, 2016"
    },
    {
        "Lead Title": "Windows 10 Deal",
        "Value" : "Rs 1,98,000",
        "Stage" : "Leads",
        "Company" : "Microsoft",
        "Contact" : "Anju Yadav",
        "Expected Close Date" : "29th January, 2016"
    },
    {
        "Lead Title": "Windows 10 Deal",
        "Value" : "Rs 1,98,000",
        "Stage" : "Leads",
        "Company" : "Microsoft",
        "Contact" : "Anju Yadav",
        "Expected Close Date" : "29th January, 2016"
    },
    {
        "Lead Title": "Windows 10 Deal",
        "Value" : "Rs 1,98,000",
        "Stage" : "Leads",
        "Company" : "Microsoft",
        "Contact" : "Anju Yadav",
        "Expected Close Date" : "29th January, 2016"
    },
    {
        "Lead Title": "Windows 10 Deal",
        "Value" : "Rs 1,98,000",
        "Stage" : "Leads",
        "Company" : "Microsoft",
        "Contact" : "Anju Yadav",
        "Expected Close Date" : "29th January, 2016"
    },
    {
        "Lead Title": "Windows 10 Deal",
        "Value" : "Rs 1,98,000",
        "Stage" : "Leads",
        "Company" : "Microsoft",
        "Contact" : "Anju Yadav",
        "Expected Close Date" : "29th January, 2016"
    },
    {
        "Lead Title": "Windows 10 Deal",
        "Value" : "Rs 1,98,000",
        "Stage" : "Leads",
        "Company" : "Microsoft",
        "Contact" : "Anju Yadav",
        "Expected Close Date" : "29th January, 2016"
    },
    {
        "Lead Title": "Windows 10 Deal",
        "Value" : "Rs 1,98,000",
        "Stage" : "Leads",
        "Company" : "Microsoft",
        "Contact" : "Anju Yadav",
        "Expected Close Date" : "29th January, 2016"
    },
    {
        "Lead Title": "Windows 10 Deal",
        "Value" : "Rs 1,98,000",
        "Stage" : "Leads",
        "Company" : "Microsoft",
        "Contact" : "Anju Yadav",
        "Expected Close Date" : "29th January, 2016"
    },
    {
        "Lead Title": "Windows 10 Deal",
        "Value" : "Rs 1,98,000",
        "Stage" : "Leads",
        "Company" : "Microsoft",
        "Contact" : "Anju Yadav",
        "Expected Close Date" : "29th January, 2016"
    },
    {
        "Lead Title": "Windows 10 Deal",
        "Value" : "Rs 1,98,000",
        "Stage" : "Leads",
        "Company" : "Microsoft",
        "Contact" : "Anju Yadav",
        "Expected Close Date" : "29th January, 2016"
    },
    {
        "Lead Title": "Windows 10 Deal",
        "Value" : "Rs 1,98,000",
        "Stage" : "Leads",
        "Company" : "Microsoft",
        "Contact" : "Anju Yadav",
        "Expected Close Date" : "29th January, 2016"
    },
	];
 
    $scope.gridOptions1.data = testData;
    $scope.gridOptions2.data = testData;
 
}]);