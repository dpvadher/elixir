$(function() {
  setTimeout(function(){
    var winWidth = $(window).width();
    if(!$('.stages-list').parent().hasClass('deal-short-detail')){
      $('.stages-list li').width(($('.content-wrapper').width() / $('.stages-list li').length)-5);
    }
    $(window).resize(function(){
      winWidth = $(window).width();
      $('.stages-list li').width($('.content-wrapper').width() / $('.stages-list li').length);
      if($(window).width() < 1114){
        $('.navbar li .visible-lg').addClass('custom-visible');
      }else{
        $('.navbar li .visible-lg').removeClass('custom-visible');
      }
    });
    $(".all-task").sortable({
      connectWith: ".all-task",
      handle: ".white-box",
      placeholder: "portlet-placeholder",
      cursorAt: { top: 80 },
      refreshPositions: true,
      containment: '.stages-list',
      start: function(event,ui){
        $('.task-action-wrapper').addClass('open');
      },
      over: function(event, ui){
        $('.stages-list li').removeClass('active');
        $('.portlet-placeholder').closest('li').addClass('active');
      },
      stop: function(event,ui){
        $('.stages-list li').removeClass('active');
        $('.task-action-wrapper').removeClass('open');
        setAllTaskHeight();
      }
    });
    $('.stages-list').resizable({
      handles: 'e'
    });
    $('.trash').droppable({
      drop: function(event, ui) {
        ui.draggable.remove();
        $('.trash').removeClass('active');
        setAllTaskHeight();
      },
      over: function(){
        $('.trash').addClass('active');
      },
      out: function(event,ui){
        $('.trash').removeClass('active');
      }
    });

    $('.thumb-up').droppable({
        over: function(event,ui){
          $('.thumb-up').addClass('active');
        },
        out: function(event,ui){
          $('.thumb-up').removeClass('active');
        },
        drop: function(event, ui) {
          $('.thumb-up').removeClass('active');
        }
    });

    $('.thumb-down').droppable({
        over: function(event,ui){
          $('.thumb-down').addClass('active');
        },
        out: function(event,ui){
          $('.thumb-down').removeClass('active');
        },
        drop: function(event, ui) {
          $('.thumb-down').removeClass('active');
        }
    });

    $('.dropdown-menu.table-menu li a').on('click',function(){
      $(this).closest('ul').find('li a').removeClass('active');
      $(this).closest('ul').prev().find('span.text').html($(this).html());
      $(this).addClass('active');
    });

    $('.profile-pic a').on('click',function(){
      $('.profile-menu').fadeIn();
      $('.profile-dropdown').animate({right:"0px"},500);
    });

    $('.close-profile-dd').on('click',function(){
      $('.profile-dropdown').animate({right:"-310px"},300,function(){
        $('.profile-menu').fadeOut();
      });
    });

    $('.close-notification-dd').on('click',function(){
      $('.notification-full-view').animate({right:"-430px"},300,function(){
        $('.notification-overlay').fadeOut();
      });
    });

    $('.notifications-full-view-link').on('click',function(){
      $('.notification-overlay').fadeIn();
      $('.notification-full-view').animate({right:"0px"},500);
    });

    $('[data-toggle="tooltip"]').tooltip();

    if($('.stages-list').length){
      setAllTaskHeight();
    }

    // setTimeout(function(){
    //   $('.popup-notification-wrapper').addClass('open');
    // },2000);

    $('.notification-icon-close').on('click',function(){
      $('.popup-notification-wrapper').removeClass('open');
    });

    function setAllTaskHeight(){
      var maxList = 0;
      var calculatedHeight = 0;
      $('.stages-list li').each(function(){
        var sortableDivLength = $(this).find('.sortable-div').length;
        if(sortableDivLength > maxList){
          maxList = sortableDivLength;
        }
      });
      $('.all-task').height($('body').height() - (maxList*104));
      calculatedHeight = $('body').height() - $('.all-task').offset().top;
      $('.all-task').height(calculatedHeight+120+106);
    }

    $('.stages-wrapper li').on('click',function(){
      $(this).addClass('active');
      $(this).prevAll().addClass('active');
      $(this).nextAll().removeClass('active');
    });

    $('.selectpicker').selectpicker();
  },1000);
});
