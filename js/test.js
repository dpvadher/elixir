$(function(){
        function Vehicle() {
		    var registration = Math.random();
		    var prop = {
		        get: function() {
		            return registration
		        }
		    }
		    Object.defineProperty(this, "registration", prop)
		}

		//land vehicle travel on land, hence it has tires
		function LandVehicle(numberOfTires, make) {
		    Vehicle.call(this);
		    if (!numberOfTires || isNaN(numberOfTires)) {
		        return;
		    }

		    var tires = [];
		    for (var i = 1; i <= numberOfTires; i++) {
		        var tire = new Tire(Math.random());
		        tires.push(tire);
		    }

		    var prop = {
		        get: function() {
		            return tires
		        }
		    }
		    Object.defineProperty(this, "tires", prop)

		    var prop = {
		        get: function() {
		            return make
		        }
		    }
		    Object.defineProperty(this, "make", prop)

		    //Tire factory
		    function Tire(pressure) {
		        var prop = {
		            get: function() {
		                return pressure
		            }
		        }
		        Object.defineProperty(this, "pressure", prop)
		    }
		}

		function MotorBike() {
		    var make = MotorBike.randomMake();
		    LandVehicle.call(this, 2, make);
		    this.topSpeed = 250;
		}
		//static functions
		Object.defineProperty(MotorBike, "makes", {
		    get: function() {
		        return ["Honda", "Kawasaki", "Suzuki", "Yamaha"]
		    }
		});

		Object.defineProperty(MotorBike, "randomMake", {
		    value: getRandomMake
		})

		function getRandomMake() {
		    var rand = Math.round(this.makes.length * Math.random()) % this.makes.length;
		    return this.makes[rand];
		}

		function Car() {
		    //random make
		    var make = Car.randomMake();
		    LandVehicle.call(this, 4, make);
		}

		//static functions
		Object.defineProperty(Car, "makes", {
		    get: function() {
		        return ["ferrari", "audi", "bmw", "honda", "ford"]
		    }
		})

		Object.defineProperty(Car, "randomMake", {
		    value: getRandomMake
		})

		function SportsCar() {
		    Car.call(this)
		    this.topSpeed = 300;
		}

		function RacingCar() {
		    SportsCar.call(this);
		    this.topSpeed = 415;
		}

		LandVehicle.prototype = new Vehicle();
		Car.prototype = new LandVehicle();
		MotorBike.prototype = new LandVehicle();
		SportsCar.prototype = new Car();
		RacingCar.prototype = new SportsCar();

		//now we are free to set other prototypes . 
		Object.defineProperty(MotorBike.prototype, "ride", {
		    value: function() {
		        console.log("Vroom Vrooooom Vrooom VROOOOM");
		    }
		});

		Object.defineProperty(Vehicle.prototype, "topSpeed", {
		    value: 20,
		    writable: true
		});

		Object.defineProperty(Car.prototype, "horn", {
		    value: function() {
		        console.log("Honk!")
		    }
		});

		Object.defineProperty(Car.prototype, "drive", {
		    value: function() {
		        console.log("Vroom!")
		    }
		});

		Object.defineProperty(SportsCar.prototype, "overtake", {
		    value: function() {
		        console.log("overtaking!")
		    }
		});

		Object.defineProperty(RacingCar.prototype, "race", {
		    value: function() {
		        console.log("racing!")
		    }
		});

		//formula1 cars
		var f1 = new RacingCar();
		var f2 = new RacingCar();

		//sports cars
		var sports1 = new SportsCar();
		var sports2 = new SportsCar();

		//car
		var car1 = new Car();
		var car2 = new Car();

		//Motorbikes
		var bike1 = new MotorBike();
		var bike2 = new MotorBike();

		console.log(f1)
		console.log(f2)
		console.log(sports1)
		console.log(sports2)
		console.log(car1)
		console.log(car2)
		console.log(bike1)
		console.log(bike2)
      })